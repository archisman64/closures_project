function limitFunctionCallCount(cb, n) {
    if(typeof cb !== 'function' || typeof n !== 'number') {
        throw new Error('invalid input type');
    }
    let callCount = 0;
    return function invokeCallback(...args) {
        console.log('invoker called');
        if(callCount < n){
            console.log('callback invoked');
            callCount ++;
            return cb(...args);
        } else {
            console.log('maximum call limit reached');
            return null;
        }
    }
    // Should return a function that invokes `cb`.
    // The returned function should only allow `cb` to be invoked `n` times.
    // Returning null is acceptable if cb can't be returned
}

module.exports = limitFunctionCallCount;