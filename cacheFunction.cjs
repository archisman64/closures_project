function cacheFunction(cb) {
    if(typeof cb !== 'function') {
        throw 'supplied parameter is of incorrect type';
    }
    const cache = {};

    return function invokeCallback(...args) {
        const key = JSON.stringify(args);
        // check for the key in cache object and if it is not found use stringyfied key
        // as key and the callback's value as value for creating a new entry in the cache..
        if(!(key in cache)) {
            console.log('new entry created');
            cache[key] = cb(...args);
        }
        console.log('cache:', cache);
        return cache[key];
    }
    // Should return a function that invokes `cb`.
    // A cache (object) should be kept in closure scope.
    // The cache should keep track of all arguments have been used to invoke this function.
    // If the returned function is invoked with arguments that it has already seen
    // then it should return the cached result and not invoke `cb` again.
    // `cb` should only ever be invoked once for a given set of arguments.
}

module.exports = cacheFunction;