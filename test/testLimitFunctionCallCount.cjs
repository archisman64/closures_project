const limitFunctionCallCount = require('../limitFunctionCallCount.cjs');
// get the function which invokes the callback;
try {
    
    const invokeCallback = limitFunctionCallCount(function (a,b) {
        console.log('arguments:', a, b);
        console.log('I am callback, thanks for calling me!');
    }, 3);
    
    // invoking the callback multiple times
    console.log(invokeCallback('a','b'));
    console.log(invokeCallback('a','b'));
    console.log(invokeCallback('a','b'));
    console.log(invokeCallback('a','b'));
} catch (error) {
    console.log(error)
}