const cacheFunction = require('../cacheFunction.cjs');

function callback(...nums) {
    // console.log('got inside callback');
    let sum = 0;
    for(let index = 0; index< nums.length; index++) {
        sum += nums[index];
    }
    return sum;
}
const invoker = cacheFunction(callback);

console.log(invoker(1,2,3)); // invokes the callback and creates new entry in cache
console.log(invoker(10,20,30)); // "  "  "
console.log(invoker(1,2,3));// fetches the prewritten value from cache object and returns it
console.log(invoker(13248,1347019,123413));// invokes the callback and creates new entry in cache
console.log(invoker(13248,1347019,123413));// fetches the prewritten value from cache object and returns it