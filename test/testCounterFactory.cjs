const counterFactory = require('../counterFactory.cjs');
// invoking counterFactory to get the enclosed counter object
const counter = counterFactory();
console.log(counter.decrement());
console.log(counter.increment());
console.log(counter.increment());
console.log(counter.increment());
console.log(counter.decrement());
console.log(counter.decrement());